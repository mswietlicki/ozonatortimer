#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>

#define GetBit(var,pos) ((var) & (1<<(pos)))
#define SetBit(var,pos,val) if(val) var |= _BV(pos); else var &= ~_BV(pos)

#define SetPinOutput(pin) \
{ \
	PORTB &= ~_BV(pin); \
	DDRB |= _BV(pin); \
}

uint8_t bigcounter = 80;
uint8_t counter = 36;

int main(void){
	DDRB = 0x00;
	PORTB = 0xFF;

	SetPinOutput(3);

	while(1){

		SetBit(PORTB, 3, 1);
		_delay_ms(500);
		SetBit(PORTB, 3, 0);

		while(counter-- > 0)
			_delay_ms(10000);

		counter = 36;
		bigcounter--;
		
		while(bigcounter == 0)
			_delay_ms(10);
	}
	return 0;
}